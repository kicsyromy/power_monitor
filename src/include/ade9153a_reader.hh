#pragma once

#include "common.hh"
#include "readings.hh"
#include "sample_buffer.hh"
#include "signal.hh"

#include <ade9153a.h>
#include <ade9153aapi.h>

#ifdef __xtensa__
#include <FreeRTOS.h>
#include <esp_timer.h>
#include <freertos/semphr.h>
#else
using esp_timer_handle_t = void *;
#endif

#include <chrono>
#include <memory>

struct ADE9153AReader
{
public:
    ADE9153AReader() noexcept;
    void start_reading() noexcept;

signals:
    Signal<std::vector<std::uint8_t> &&> readings_ready{};

private:
    void run_calibration() noexcept;
    void update_readings() noexcept;

private:
    static void timer_callback(void *instance) noexcept;
    [[noreturn]] static void *read_instance_values(void *instance) noexcept;

private:
    ADE9153AClass ade9153a_{};

    InstantRegs instant_registers_{};
    PowerRegs power_registers_{};
    PQRegs power_quality_registers_{};
    EnergyRegs energy_regs_{};

    esp_timer_handle_t read_timer_{};

    using Buffer = SampleBuffer<float, SAMPLE_BUFFER_SIZE>;
    Buffer current_samples_{};
    Buffer voltage_samples_{};

    Readings readings_{};

    void *read_instant_values_task_{};
    void *read_instant_values_lock_{};
};

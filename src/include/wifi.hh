#pragma once

#include "common.hh"
#include "signal.hh"

#include <cstdint>
#include <string_view>

#ifdef __xtensa__

#include <esp_event.h>
#include <esp_wifi.h>
#include <freertos/event_groups.h>

using ESPEventBase = esp_event_base_t;
using WiFiConfig = wifi_config_t;
using EventGroupHandle = EventGroupHandle_t;
using EventHandlerInstance = esp_event_handler_instance_t;
#else
using ESPEventBase = const char *;
using WiFiConfig = void *;
using EventGroupHandle = void *;
using EventHandlerInstance = void *;
#endif /* __xtensa__ */

struct WiFi
{
    enum struct AuthenticationMode
    {
        Open = 0,
        WEP = 1,
        WPA2_PSK = 3,
        WPA_WPA2_PSK = 4,
        WPA3_PSK = 6,
        WPA2_WPA3_PSK = 7,
    };

    WiFi() noexcept;
    ~WiFi() noexcept;

    void set_ssid(std::string_view ssid) noexcept;
    void set_password(std::string_view password) noexcept;
    void set_authentication(AuthenticationMode mode) noexcept;

    void connect() noexcept;
    void disconnect() noexcept;

    inline bool is_connected() const noexcept
    {
        return is_connected_;
    }

signals:
    Signal<std::uint32_t /* ip_address */> connected{};
    Signal<> disconnected{};
    Signal<> connection_failed{};

private:
    static void event_handler(void *instance,
        ESPEventBase event_base,
        std::int32_t event_id,
        void *event_data);

private:
    WiFiConfig config_{};
    EventGroupHandle event_group_{};
    EventHandlerInstance event_handler_instance_wifi_{};
    EventHandlerInstance event_handler_instance_ip_{};

private:
    std::uint8_t ongoing_retry_count_{ 0 };
    bool is_connected_{ false };

private:
    CM_DISABLE_COPY(WiFi);
    CM_DISABLE_MOVE(WiFi);
};
#pragma once

#include <algorithm>
#include <array>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <vector>

#define DECLTYPE_FROM(type_name, underlying_type)                  \
    struct type_name                                               \
    {                                                              \
        using UnderlyingType = underlying_type;                    \
        constexpr type_name() noexcept = default;                  \
        constexpr explicit type_name(underlying_type v) noexcept   \
          : value_{ v }                                            \
        {}                                                         \
        constexpr type_name &operator=(underlying_type v) noexcept \
        {                                                          \
            value_ = v;                                            \
            return *this;                                          \
        }                                                          \
        constexpr operator underlying_type() const noexcept        \
        {                                                          \
            return value_;                                         \
        }                                                          \
                                                                   \
    private:                                                       \
        underlying_type value_{};                                  \
    }

template<typename T, std::size_t precision, typename Decimal>
constexpr auto to_internal_representation(Decimal value) noexcept
{
    for (std::size_t i = 0; i < precision; ++i)
    {
        value *= 10;
    }

    auto v = std::floor(value);

    const auto result =
        static_cast<typename T::UnderlyingType>(v < 0 ? std::ceil(value) : std::floor(value));

    // Account for conversion error/overflow when very close to limits (+ => -)
    if (result < 0 && value > 0)
    {
        return std::numeric_limits<typename T::UnderlyingType>::max();
    }

    // Account for conversion error/overflow when very close to limits (- => +)
    if (result > 0 && value < 0)
    {
        return std::numeric_limits<typename T::UnderlyingType>::min();
    }

    return result;
}

template<typename T> constexpr void set_value(T &v, float value) noexcept
{
    v.value = to_internal_representation<typename T::Type, T::PRECISION>(value);
}

struct Readings
{
    DECLTYPE_FROM(Volt, std::int32_t);
    DECLTYPE_FROM(Amp, std::int32_t);
    DECLTYPE_FROM(Watt, std::int32_t);
    DECLTYPE_FROM(VoltAmp, std::int32_t);
    DECLTYPE_FROM(VoltAmpReactive, std::int32_t);
    DECLTYPE_FROM(WattPerVoltAmp, std::int8_t);
    DECLTYPE_FROM(WattHour, std::uint64_t);
    DECLTYPE_FROM(VoltAmpHour, std::int64_t);
    DECLTYPE_FROM(VoltAmpReactiveHour, std::int64_t);
    DECLTYPE_FROM(Hertz, std::uint16_t);
    DECLTYPE_FROM(Degrees, std::int16_t);
    DECLTYPE_FROM(Percentage, std::uint16_t);

    using Complex = float[2];

    template<typename T, std::uint8_t precision> struct Unit
    {
        static constexpr auto PRECISION = precision;
        using Type = T;

        constexpr typename T::UnderlyingType base_value() const noexcept
        {
            return static_cast<typename T::UnderlyingType>(value);
        }

        inline void serialize_to(std::vector<std::uint8_t> &target) const
        {
            const auto v = base_value();
            for (auto i = static_cast<std::int8_t>(sizeof(T)) - 1; i >= 0; --i)
            {
                target.push_back(static_cast<std::uint8_t>((v >> (i * 8)) & 0xFF));
            }
        }

        inline void deserialize_from(const std::uint8_t *&input)
        {
            auto v = typename T::UnderlyingType{};
            for (auto i = static_cast<std::int8_t>(sizeof(T)) - 1; i >= 0; --i)
            {
                v = static_cast<typename T::UnderlyingType>(v | *input);
                v = static_cast<typename T::UnderlyingType>(v << (i * 8));
                ++input;
            }

            value = v;
        }

        T value;
    };

public:
    [[nodiscard]] constexpr auto rms_voltage() const noexcept;
    constexpr void set_rms_voltage(float value_milli_volt) noexcept;

    [[nodiscard]] constexpr auto rms_current() const noexcept;
    constexpr void set_rms_current(float value_milli_amp) noexcept;

    [[nodiscard]] constexpr auto total_active_power() const noexcept;
    constexpr void set_total_active_power(float value_milli_watt) noexcept;

    [[nodiscard]] constexpr auto apparent_power() const noexcept;
    constexpr void set_apparent_power(float value_milli_volt_amp) noexcept;

    [[nodiscard]] constexpr auto fundamental_reactive_power() const noexcept;
    constexpr void set_fundamental_reactive_power(float value_milli_volt_amp_reactive) noexcept;

    [[nodiscard]] constexpr auto power_factor() const noexcept;
    constexpr void set_power_factor(float value) noexcept;

    [[nodiscard]] constexpr auto total_active_energy() const noexcept;
    constexpr void set_total_active_energy(float value_milli_watt_hour) noexcept;

    [[nodiscard]] constexpr auto apparent_energy() const noexcept;
    constexpr void set_apparent_energy(float value_milli_milli_volt_amp_hour) noexcept;

    [[nodiscard]] constexpr auto fundamental_reactive_energy() const noexcept;
    constexpr void set_fundamental_reactive_energy(
        float value_milli_volt_amp_reactive_hour) noexcept;

    [[nodiscard]] constexpr auto frequency() const noexcept;
    constexpr void set_frequency(float value_hz) noexcept;

    [[nodiscard]] constexpr auto phi() const noexcept;
    constexpr void set_phi(float value_degrees) noexcept;

    [[nodiscard]] constexpr auto voltage_thd() const noexcept;
    constexpr void set_voltage_thd(float value) noexcept;

    [[nodiscard]] constexpr auto current_thd() const noexcept;
    constexpr void set_current_thd(float value) noexcept;

    constexpr void set_voltage_hd(const Complex *values, std::size_t count) noexcept;

    constexpr void set_current_hd(const Complex *values, std::size_t count) noexcept;

    [[nodiscard]] std::vector<std::uint8_t> to_binary();
    [[nodiscard]] static Readings from_binary(const std::uint8_t *payload) noexcept;

private:
    static constexpr std::pair<std::size_t, float> find_fundamental_freq(const Complex *values,
        std::size_t count) noexcept;

    static constexpr float find_next_frequency(std::size_t index,
        std::size_t min_offset,
        std::size_t max_offset,
        const Complex *values,
        std::size_t count) noexcept;

private:
    Unit<Volt, 0> rms_voltage_{};
    Unit<Amp, 0> rms_current_{};
    Unit<Watt, 0> total_active_power_{};
    Unit<VoltAmp, 0> apparent_power_{};
    Unit<VoltAmpReactive, 0> fundamental_reactive_power_{};
    Unit<WattPerVoltAmp, 2> power_factor_{};
    Unit<WattHour, 0> total_active_energy_{};
    Unit<VoltAmpHour, 0> apparent_energy_{};
    Unit<VoltAmpReactiveHour, 0> fundamental_reactive_energy_{};
    Unit<Hertz, 3> frequency_{};
    Unit<Degrees, 2> phi_{};
    Unit<Percentage, 2> voltage_thd_{};
    Unit<Percentage, 2> current_thd_{};
    std::array<Unit<Percentage, 2>, 40> voltage_hd_{};
    std::array<Unit<Percentage, 2>, 40> current_hd_{};
};

constexpr auto Readings::rms_voltage() const noexcept
{
    return rms_voltage_.value;
}

constexpr void Readings::set_rms_voltage(float value) noexcept
{
    set_value(rms_voltage_, value);
}

constexpr auto Readings::rms_current() const noexcept
{
    return rms_current_.value;
}

constexpr void Readings::set_rms_current(float value) noexcept
{
    set_value(rms_current_, value);
}

constexpr auto Readings::total_active_power() const noexcept
{
    return total_active_power_.value;
}

constexpr void Readings::set_total_active_power(float value) noexcept
{
    set_value(total_active_power_, value);
}

constexpr auto Readings::apparent_power() const noexcept
{
    return apparent_power_.value;
}

constexpr void Readings::set_apparent_power(float value) noexcept
{
    set_value(apparent_power_, value);
}

constexpr auto Readings::fundamental_reactive_power() const noexcept
{
    return fundamental_reactive_power_.value;
}

constexpr void Readings::set_fundamental_reactive_power(float value) noexcept
{
    set_value(fundamental_reactive_power_, value);
}

constexpr auto Readings::power_factor() const noexcept
{
    return power_factor_.value;
}

constexpr void Readings::set_power_factor(float value) noexcept
{
    set_value(power_factor_, value);
}

constexpr auto Readings::total_active_energy() const noexcept
{
    return total_active_energy_.value;
}

constexpr void Readings::set_total_active_energy(float value) noexcept
{
    set_value(total_active_energy_, value);
}

constexpr auto Readings::apparent_energy() const noexcept
{
    return apparent_energy_.value;
}

constexpr void Readings::set_apparent_energy(float value) noexcept
{
    set_value(apparent_energy_, value);
}

constexpr auto Readings::fundamental_reactive_energy() const noexcept
{
    return fundamental_reactive_energy_.value;
}

constexpr void Readings::set_fundamental_reactive_energy(float value) noexcept
{
    set_value(fundamental_reactive_energy_, value);
}

constexpr auto Readings::frequency() const noexcept
{
    return frequency_.value;
}

constexpr void Readings::set_frequency(float value) noexcept
{
    set_value(frequency_, value);
}

constexpr auto Readings::phi() const noexcept
{
    return phi_.value;
}

constexpr void Readings::set_phi(float value) noexcept
{
    set_value(phi_, value);
}

constexpr auto Readings::voltage_thd() const noexcept
{
    return voltage_thd_.value;
}

constexpr void Readings::set_voltage_thd(float value) noexcept
{
    set_value(voltage_thd_, value);
}

constexpr auto Readings::current_thd() const noexcept
{
    return current_thd_.value;
}

constexpr void Readings::set_current_thd(float value) noexcept
{
    set_value(current_thd_, value);
}

constexpr void Readings::set_voltage_hd(const Complex *values, std::size_t count) noexcept
{
    const auto [fundamental_index, fundamental] = find_fundamental_freq(values, count);

    auto thd = float{};
    for (std::size_t i = fundamental_index, j = 0; i < count && j < voltage_hd_.size();
         i += fundamental_index, ++j)
    {
        const auto v = find_next_frequency(i, 5, 5, values, count);
        set_value(voltage_hd_[j], (v / fundamental));
        if (j != 0)
        {
            thd += v * v;
        }
    }

    if (fundamental != 0)
    {
        thd = std::sqrt(thd) / fundamental;
    }
    else
    {
        thd = .0F;
    }
    set_voltage_thd(thd);

    std::printf("V THD: %f\n", static_cast<double>(thd));
    std::printf("V Fundamental: %f\n", static_cast<double>(fundamental));
    std::printf("V Fundamental Index: %d\n", fundamental_index);
}

constexpr void Readings::set_current_hd(const Complex *values, std::size_t count) noexcept
{
    const auto [fundamental_index, fundamental] = find_fundamental_freq(values, count);

    auto thd = float{};
    for (std::size_t i = fundamental_index, j = 0; i < count && j < current_hd_.size();
         i += fundamental_index, ++j)
    {
        const auto v = find_next_frequency(i, 5, 5, values, count);
        set_value(current_hd_[j], (v / fundamental));
        if (j != 0)
        {
            thd += v * v;
        }
    }

    if (fundamental != 0)
    {
        thd = std::sqrt(thd) / fundamental;
    }
    else
    {
        thd = .0F;
    }
    set_current_thd(thd);

    std::printf("A THD: %f\n", static_cast<double>(thd));
    std::printf("A Fundamental: %f\n", static_cast<double>(fundamental));
    std::printf("A Fundamental Index: %d\n", fundamental_index);
}

constexpr std::pair<std::size_t, float> Readings::find_fundamental_freq(const Complex *values,
    std::size_t count) noexcept
{
    const auto max_i = std::min(count, std::size_t{ 60 });

    auto index = std::size_t{ 0 };
    auto value = float{ 0.F };

    for (std::size_t i = 40; i < max_i; ++i)
    {
        const auto v = std::abs(values[i][0]);
        if (v > value)
        {
            value = v;
            index = i;
        }
    }

    return { index, value };
}

constexpr float Readings::find_next_frequency(std::size_t index,
    std::size_t min_offset,
    std::size_t max_offset,
    const Complex *values,
    std::size_t count) noexcept
{
    auto value = values[index][0];

    min_offset = index - min_offset;
    max_offset = index + max_offset;
    const auto max_i = std::min(count, max_offset);

    for (std::size_t i = min_offset; i < max_i; ++i)
    {
        const auto v = std::abs(values[i][0]);
        if (v > value)
        {
            value = v;
            index = i;
        }
    }

    return value;
}

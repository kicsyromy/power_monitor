#pragma once

#ifdef __xtensa__
#include <esp_timer.h>
#else
#include <thread>
#endif

#include <hedley/hedley.h>

#include <array>
#include <chrono>
#include <cstdint>
#include <string>
#include <string_view>
#include <tuple>

#ifdef CONFIG_CM_WIFI_SSID
constexpr std::string_view WIFI_SSID{ CONFIG_CM_WIFI_SSID };
#else
constexpr std::string_view WIFI_SSID{ "" };
#endif

#ifdef CONFIG_CM_WIFI_PASSWORD
constexpr std::string_view WIFI_PASSWORD{ CONFIG_CM_WIFI_PASSWORD };
#else
constexpr std::string_view WIFI_PASSWORD{ "" };
#endif

#ifdef CONFIG_CM_WIFI_MAXIMUM_RETRIES
constexpr std::uint8_t WIFI_MAXIMUM_RETRIES{ CONFIG_CM_WIFI_MAXIMUM_RETRIES };
#else
constexpr std::uint8_t WIFI_MAXIMUM_RETRIES{ 5 };
#endif

#ifdef CONFIG_CM_MQTT_BROKER_URL
constexpr std::string_view MQTT_BROKER_URL{ CONFIG_CM_MQTT_BROKER_URL };
#else
constexpr std::string_view MQTT_BROKER_URL{
    "mqtt://f96c1ea9d3924042871342ee37420662.s1.eu.hivemq.cloud"
};
#endif

#ifdef CONFIG_CM_MQTT_TLS_PORT
constexpr auto MQTT_TLS_PORT = std::uint32_t{ CONFIG_CM_MQTT_TLS_PORT };
#else
constexpr auto MQTT_TLS_PORT = std::uint32_t{ 8883 };
#endif

#ifdef CONFIG_CM_MQTT_USERNAME
constexpr std::string_view MQTT_USERNAME{ CONFIG_CM_MQTT_USERNAME };
#else
constexpr std::string_view MQTT_USERNAME{ "mqttest" };
#endif

#ifdef CONFIG_CM_MQTT_PASSWORD
constexpr std::string_view MQTT_PASSWORD{ CONFIG_CM_MQTT_PASSWORD };
#else
constexpr std::string_view MQTT_PASSWORD{ "Zomfg1234" };
#endif

#ifdef CONFIG_CM_SAMPLE_BUFFER_SIZE
constexpr auto SAMPLE_BUFFER_SIZE = static_cast<std::size_t>(CONFIG_CM_SAMPLE_BUFFER_SIZE);
#else
constexpr std::size_t SAMPLE_BUFFER_SIZE{ 4096 };
#endif

#define CM_DISABLE_COPY(klass)              \
    explicit klass(const klass &) = delete; \
    klass &operator=(const klass &) = delete

#define CM_DISABLE_MOVE(klass)         \
    explicit klass(klass &&) = delete; \
    klass &operator=(klass &&) = delete

#define CM_UNUSED(var) static_cast<void>(var)

#ifdef __clang__
#define CM_DIAGNOSTIC_IGNORE_OLD_STYLE_CAST \
    HEDLEY_PRAGMA(clang diagnostic ignored "-Wold-style-cast")
#define CM_DIAGNOSTIC_IGNORE_USELESS_CAST HEDLEY_PRAGMA(clang diagnostic ignored "-Wuseless-cast")
#elif __GNUG__
#define CM_DIAGNOSTIC_IGNORE_OLD_STYLE_CAST HEDLEY_PRAGMA(GCC diagnostic ignored "-Wold-style-cast")
#define CM_DIAGNOSTIC_IGNORE_USELESS_CAST HEDLEY_PRAGMA(GCC diagnostic ignored "-Wuseless-cast")
#else
#define CM_DIAGNOSTIC_IGNORE_OLD_STYLE_CAST
#define CM_DIAGNOSTIC_IGNORE_USELESS_CAST
#endif

constexpr auto APP_TAG{ "PowerMonitor" };

using TimePoint = std::chrono::time_point<std::chrono::system_clock,
    std::chrono::duration<std::int64_t, std::ratio<1, 1000000000>>>;

namespace utility
{
    template<std::size_t S>
    constexpr std::size_t get_file_name_offset(const char (&path)[S], size_t i = S - 1) noexcept
    {
        return (path[i] == '/' || path[i] == '\\')
                   ? i + 1
                   : (i > 0 ? get_file_name_offset(path, i - 1) : 0);
    }

    template<typename...> struct GetClassType : std::false_type
    {
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...)>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) volatile>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const volatile>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) volatile noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const volatile noexcept>
    {
        using Type = Class;
    };

    // This is the generic case
    template<typename... T> struct Forwarder : public std::tuple<T...>
    {
        using std::tuple<T...>::tuple;
    };

    // This is the case when just one variable is being captured.
    template<typename T> struct Forwarder<T> : public std::tuple<T>
    {
        using std::tuple<T>::tuple;

        // Pointer-like accessors
        auto &operator*()
        {
            return std::get<0>(*this);
        }

        const auto &operator*() const
        {
            return std::get<0>(*this);
        }

        auto *operator->()
        {
            return &std::get<0>(*this);
        }

        const auto *operator->() const
        {
            return &std::get<0>(*this);
        }
    };

    // The below two functions declarations are used by the deduction guide
    // to determine whether to copy or reference the variable
    template<typename T> T Forwarder_type(const T &);

    template<typename T> T &Forwarder_type(T &);

    // Here comes the deduction guide
    template<typename... T>
    Forwarder(T &&...t) -> Forwarder<decltype(Forwarder_type(std::forward<T>(t)))...>;

    inline void delay_microseconds(std::uint32_t value)
    {
#ifdef __xtensa__
        const auto m = static_cast<std::uint64_t>(esp_timer_get_time());
        if (value)
        {
            const auto e = uint64_t{ m + value };
            if (m > e)
            { // overflow
                while (static_cast<std::uint64_t>(esp_timer_get_time()) > e)
                {
                    __asm volatile("nop");
                }
            }
            while (static_cast<std::uint64_t>(esp_timer_get_time()) < e)
            {
                __asm volatile("nop");
            }
        }
#else
        std::this_thread::sleep_for(std::chrono::microseconds{ value });
#endif
    }

    inline std::string base64_encode(unsigned char const *bytes_to_encode, std::size_t count)
    {
        size_t len_encoded = (count + 2) / 3 * 4;

        constexpr unsigned char trailing_char = '=';

        //
        // Choose set of base64 characters. They differ
        // for the last two positions, depending on the url
        // parameter.
        // A bool (as is the parameter url) is guaranteed
        // to evaluate to either 0 or 1 in C++ therefore,
        // the correct character set is chosen by subscripting
        // base64_chars with url.
        //
        constexpr auto base64_chars_ = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                       "abcdefghijklmnopqrstuvwxyz"
                                       "0123456789"
                                       "+/";

        std::string ret;
        ret.reserve(len_encoded);

        unsigned int pos = 0;
        while (pos < count)
        {
            ret.push_back(base64_chars_[(bytes_to_encode[pos + 0] & 0xfc) >> 2]);

            if (pos + 1 < count)
            {
                ret.push_back(base64_chars_[((bytes_to_encode[pos + 0] & 0x03) << 4) +
                                            ((bytes_to_encode[pos + 1] & 0xf0) >> 4)]);

                if (pos + 2 < count)
                {
                    ret.push_back(base64_chars_[((bytes_to_encode[pos + 1] & 0x0f) << 2) +
                                                ((bytes_to_encode[pos + 2] & 0xc0) >> 6)]);
                    ret.push_back(base64_chars_[bytes_to_encode[pos + 2] & 0x3f]);
                }
                else
                {
                    ret.push_back(base64_chars_[(bytes_to_encode[pos + 1] & 0x0f) << 2]);
                    ret.push_back(trailing_char);
                }
            }
            else
            {

                ret.push_back(base64_chars_[(bytes_to_encode[pos + 0] & 0x03) << 4]);
                ret.push_back(trailing_char);
                ret.push_back(trailing_char);
            }

            pos += 3;
        }

        return ret;
    }
} // namespace utility

// std::tuple_size needs to be specialized for our type,
// so that std::apply can be used.
namespace std
{
    template<typename... T> struct tuple_size<utility::Forwarder<T...>> : tuple_size<tuple<T...>>
    {
    };
} // namespace std
#include "ade9153a_reader.hh"
#include "logger.hh"

#include <arduino_compatibility/utilities.hh>
#include <hedley/hedley.h>
#include <minfft.h>

#ifdef __xtensa__
#include <driver/spi_master.h>
#include <esp_pthread.h>
#else
#include <thread>

#define xSemaphoreGive(...)
#define xSemaphoreTake(...) true
#endif

#include <pthread.h>

#include <bitset>
#include <cstdint>
#include <numeric>

#define PTHREAD_CHECK(result, message)                                   \
    do                                                                   \
    {                                                                    \
        if (result != 0)                                                 \
        {                                                                \
            CM_LOG_ERROR_F(message ". pthread API returned {}", result); \
            for (;;)                                                     \
                ;                                                        \
        }                                                                \
    } while (false)

namespace
{
    constexpr std::uint32_t SPI_SPEED{ 2000000 };
    constexpr std::uint8_t CS_PIN{ 5 };
    constexpr std::uint8_t RESET_PIN{ 26 };

    inline void reset_ade9153a() noexcept
    {
        CM_LOG_INFO("Resetting ADE9153A...");

        digitalWrite(RESET_PIN, LOW);
        delay(100);
        digitalWrite(RESET_PIN, HIGH);
        delay(1000);

        CM_LOG_INFO("Reset complete");
    }

    inline void init_timer(void (*callback)(void *),
        void *callback_argument,
        esp_timer_handle_t &timer) noexcept
    {
#ifdef __xtensa__
        auto timer_config = esp_timer_create_args_t{};
        timer_config.callback = callback;
        timer_config.arg = callback_argument;
        timer_config.dispatch_method = ESP_TIMER_ISR;
        timer_config.name = "ADE9153A";
        timer_config.skip_unhandled_events = true;

        esp_timer_create(&timer_config, &timer);
#else
        CM_UNUSED(callback);
        CM_UNUSED(callback_argument);
        CM_UNUSED(timer);
#endif
    }

    inline void set_up_pthread_config() noexcept
    {
#ifdef __xtensa__
        auto esp_pthread_cfg = esp_pthread_get_default_config();
        esp_pthread_cfg.pin_to_core = 1;
        esp_pthread_cfg.prio = 1;
        esp_pthread_cfg.stack_size = 256;
        esp_pthread_cfg.thread_name = "afe_reader";

        const auto result = esp_pthread_set_cfg(&esp_pthread_cfg);
        std::printf("************************ %d\n", result);

        ESP_ERROR_CHECK(result);
#endif
    }

    inline void reset_pthread_config() noexcept
    {
#ifdef __xtensa__
        auto esp_pthread_cfg = esp_pthread_get_default_config();
        ESP_ERROR_CHECK(esp_pthread_set_cfg(&esp_pthread_cfg));
#endif
    }
} // namespace

ADE9153AReader::ADE9153AReader() noexcept
{
    pinMode(RESET_PIN, OUTPUT);
    digitalWrite(RESET_PIN, HIGH);

    pinMode(CS_PIN, OUTPUT);
    digitalWrite(CS_PIN, LOW);

    CM_LOG_INFO("Initializing ADE9153A...");
    ade9153a_.SPI_Init(SPI_SPEED, CS_PIN);
    for (;;)
    {
        reset_ade9153a();

        CM_LOG_INFO("Awaiting reset done from the device...");
        for (;;)
        {
            constexpr auto RSTDONE_BIT = std::size_t{ 16 };
            const auto status_register = std::bitset<32>{ ade9153a_.SPI_Read_32(REG_STATUS) };
            if (status_register.test(RSTDONE_BIT))
            {
                break;
            }
            delay(100);
        }
        CM_LOG_INFO("The device has signaled that reset is complete");

        const auto product_version = ade9153a_.SPI_Read_Version();
        if (product_version != 0x9153A)
        {
            CM_LOG_WARN_F("ADE9153A: Failed to initialize SPI communication. Invalid version "
                          "number 0x{:X}, expected 0x9153A",
                product_version);
            delay(1000);
        }
        else
        {
            break;
        }
    }

    ade9153a_.SetupADE9153A();
    // run_calibration();

    // TODO HACK - from Flavius
    // FIXME: Adjust current value to compensate for uncalibrated device

    /**
     * - we need to adjust the gain even more.
     * - why the payload in burst mode is not updated?
     * - improve THD, taking into consideration the range +/- 5Hz
     *
     * https://docs.google.com/spreadsheets/d/1UBb7q00l5Zne41AGs3kD4qNZV7dpOPZ0EOZ7witJguI/edit#gid=1373678789
     */

    ade9153a_.SPI_Write_32(REG_AIGAIN, static_cast<uint32_t>(-1158728183));
    delay(1000);

    CM_LOG_INFO_F("REG_AIGAIN: {}", ade9153a_.SPI_Read_32(REG_AIGAIN));

    CM_LOG_INFO("Initialized ADE9153A");

    voltage_samples_.buffer_full.connect<&ADE9153AReader::update_readings>(*this,
        pm_signal::ConnectionType::Queued);
}

void ADE9153AReader::start_reading() noexcept
{
    CM_LOG_INFO("Preparing to read data from AFE...");

    set_up_pthread_config();

    auto thread_id = pthread_t{};
    auto result = pthread_create(&thread_id, nullptr, &ADE9153AReader::read_instance_values, this);
    PTHREAD_CHECK(result, "Failed to create instant value reader thread");

    result = pthread_detach(thread_id);
    PTHREAD_CHECK(result, "Failed to detach instant value reader thread");

    init_timer(&ADE9153AReader::timer_callback, this, read_timer_);
#ifdef __xtensa__
    esp_timer_start_periodic(read_timer_, 244);
#endif

    CM_LOG_INFO("Timer and task started, readings are ongoing...");

    reset_pthread_config();
}

void ADE9153AReader::run_calibration() noexcept
{
    CM_LOG_INFO("Calibrating current channel...");
    ade9153a_.StartAcal_AINormal();
    delay(40000);
    ade9153a_.StopAcal();
    CM_LOG_INFO("Done");

    CM_LOG_INFO("Calibrating voltage channel...");
    ade9153a_.StartAcal_AV();
    delay(40000);
    ade9153a_.StopAcal();
    CM_LOG_INFO("Done");

    delay(100);

    AcalRegs acal_regs{};
    ade9153a_.ReadAcalRegs(&acal_regs);
    CM_LOG_INFO_F("AICC: {}", acal_regs.AICC);
    CM_LOG_INFO_F("AICERT: {}", acal_regs.AcalAICERTReg);
    CM_LOG_INFO_F("AVCC: {}", acal_regs.AVCC);
    CM_LOG_INFO_F("AVCERT: {}", acal_regs.AcalAVCERTReg);

    const auto current_gain = (-(static_cast<double>(acal_regs.AICC) / 838.190) - 1) * 134217728;
    const auto voltage_gain = ((static_cast<double>(acal_regs.AVCC) / 13411.05) - 1) * 134217728;

    CM_LOG_INFO_F("current_gain: {}", current_gain);
    CM_LOG_INFO_F("voltage_gain: {}", voltage_gain);

    if (acal_regs.AcalAICERTReg != 0)
    {
        ade9153a_.SPI_Write_32(REG_AIGAIN, static_cast<std::uint32_t>(current_gain));
    }
    else
    {
        CM_LOG_WARN("Failed to calibrate current channel");
    }

    if (acal_regs.AcalAVCERTReg != 0)
    {
        ade9153a_.SPI_Write_32(REG_AVGAIN, static_cast<std::uint32_t>(voltage_gain));
    }
    else
    {
        CM_LOG_WARN("Failed to calibrate voltage channel");
    }

    CM_LOG_INFO("Calibration complete");
}

void ADE9153AReader::update_readings() noexcept
{
    const auto start = std::chrono::system_clock::now();

    auto minfft_context =
        std::unique_ptr<minfft_aux, decltype(&minfft_free_aux)>{ minfft_mkaux_dft_1d(Buffer::SIZE),
            &minfft_free_aux };
    if (minfft_context == nullptr)
    {
        fprintf(stderr, "PowerMonitor: Failed to allocate FFT structure\n");
        return;
    }

    auto samples = std::vector<std::int32_t>(300);

    {
        auto *sample_buffer = voltage_samples_.inactive_buffer().data();
        for (std::size_t i = 0; i < (samples.size() / 2); ++i)
        {
            samples[i] = static_cast<std::int32_t>(std::floor(sample_buffer[i] * 100));
        }

        auto input = std::vector<minfft_cmpl>(Buffer::SIZE);
        for (std::size_t i = 0; i < Buffer::SIZE; ++i)
        {
            input[i][0] = sample_buffer[i];
            input[i][1] = .0f;
        }

        const auto total_squared_voltage =
            std::accumulate(input.cbegin(), input.cend(), 0.0, [](double sum, const float *value) {
                return sum + (static_cast<double>(value[0]) * static_cast<double>(value[0]));
            });

        auto *output = input.data();
        minfft_dft(input.data(), output, minfft_context.get());

        readings_.set_voltage_hd(output, Buffer::SIZE);

        const auto rms_voltage = static_cast<float>(sqrt(total_squared_voltage / Buffer::SIZE));
        readings_.set_rms_voltage(rms_voltage);
        std::printf("V RMS: %f\n", static_cast<double>(rms_voltage));
        std::printf("V RMS (R): %d\n", static_cast<std::int32_t>(readings_.rms_voltage()));
    }

    {
        auto *sample_buffer = current_samples_.inactive_buffer().data();
        for (std::size_t i = 0; i < (samples.size() / 2); ++i)
        {
            samples[(samples.size() / 2) + i] =
                static_cast<std::int32_t>(std::floor(sample_buffer[i] * 100));
        }

        auto input = std::vector<minfft_cmpl>(Buffer::SIZE);
        for (std::size_t i = 0; i < Buffer::SIZE; ++i)
        {
            input[i][0] = sample_buffer[i];
            input[i][1] = .0f;
        }

        const auto total_squared_current =
            std::accumulate(input.cbegin(), input.cend(), 0.0, [](double sum, const float *value) {
                return sum + (static_cast<double>(value[0]) * static_cast<double>(value[0]));
            });

        auto *output = input.data();
        minfft_dft(input.data(), output, minfft_context.get());

        readings_.set_current_hd(output, Buffer::SIZE);

        const auto rms_current = static_cast<float>(sqrt(total_squared_current / Buffer::SIZE));
        readings_.set_rms_current(rms_current);
        std::printf("A RMS: %f\n", static_cast<double>(rms_current));
        std::printf("A RMS (R): %d\n", static_cast<std::int32_t>(readings_.rms_current()));
    }

    /** POWER **/
    if (xSemaphoreTake(static_cast<QueueHandle_t>(read_instant_values_lock_), 1))
    {
        ade9153a_.ReadPowerRegs(&power_registers_);
        xSemaphoreGive(static_cast<QueueHandle_t>(read_instant_values_lock_));
    }
    readings_.set_total_active_power(power_registers_.ActivePowerValue);
    readings_.set_fundamental_reactive_power(power_registers_.FundReactivePowerValue);
    readings_.set_apparent_power(power_registers_.ApparentPowerValue);

    std::printf("ActivePower: %f\n", static_cast<double>(power_registers_.ActivePowerValue));
    std::printf("FundReactPower: %f\n",
        static_cast<double>(power_registers_.FundReactivePowerValue));
    std::printf("ApparentPower: %f\n", static_cast<double>(power_registers_.ApparentPowerValue));

    std::printf("ActivePower (R): %d\n", static_cast<std::int32_t>(readings_.total_active_power()));
    std::printf("FundReactPower (R): %d\n",
        static_cast<std::int32_t>(readings_.fundamental_reactive_power())),
        std::printf("ApparentPower (R): %d\n",
            static_cast<std::int32_t>(readings_.apparent_power()));

    /** POWER QUALITY **/
    if (xSemaphoreTake(static_cast<QueueHandle_t>(read_instant_values_lock_), 1))
    {
        ade9153a_.ReadPQRegs(&power_quality_registers_);
        xSemaphoreGive(static_cast<QueueHandle_t>(read_instant_values_lock_));
    }
    readings_.set_power_factor(power_quality_registers_.PowerFactorValue);
    readings_.set_frequency(power_quality_registers_.FrequencyValue);
    readings_.set_phi(power_quality_registers_.AngleValue_AV_AI);

    std::printf("PowerFactor: %f\n",
        static_cast<double>(power_quality_registers_.PowerFactorValue));
    std::printf("Frequency: %f\n", static_cast<double>(power_quality_registers_.FrequencyValue));
    std::printf("Phi: %f\n", static_cast<double>(power_quality_registers_.AngleValue_AV_AI));

    std::printf("PowerFactor (R): %d\n", static_cast<std::int32_t>(readings_.power_factor()));
    std::printf("Frequency (R): %d\n", static_cast<std::int32_t>(readings_.frequency()));
    std::printf("Phi (R): %d\n", static_cast<std::int32_t>(readings_.phi()));

    /** ENERGY **/
    if (xSemaphoreTake(static_cast<QueueHandle_t>(read_instant_values_lock_), 1))
    {
        ade9153a_.ReadEnergyRegs(&energy_regs_);
        xSemaphoreGive(static_cast<QueueHandle_t>(read_instant_values_lock_));
    }
    readings_.set_total_active_energy(energy_regs_.ActiveEnergyValue);
    readings_.set_fundamental_reactive_energy(energy_regs_.FundReactiveEnergyValue);
    readings_.set_apparent_energy(energy_regs_.ApparentEnergyValue);

    std::printf("ActiveEnergy: %f\n", static_cast<double>(energy_regs_.ActiveEnergyValue));
    std::printf("FundReactiveEnergy: %f\n",
        static_cast<double>(energy_regs_.FundReactiveEnergyValue));
    std::printf("ApparentEnergy: %f\n", static_cast<double>(energy_regs_.ApparentEnergyValue));

    std::printf("ActiveEnergy (R): %d\n",
        static_cast<std::int32_t>(readings_.total_active_energy()));
    std::printf("FundReactiveEnergy (R): %d\n",
        static_cast<std::int32_t>(readings_.fundamental_reactive_energy()));
    std::printf("ApparentEnergy (R): %d\n", static_cast<std::int32_t>(readings_.apparent_energy()));

    auto readings_serialized = readings_.to_binary();
    readings_serialized.insert(readings_serialized.end(),
        reinterpret_cast<std::uint8_t *>(samples.data()),
        reinterpret_cast<std::uint8_t *>(samples.data() + samples.size()));

    auto readings_hex = std::vector<char>{};
    readings_hex.reserve(2 * readings_serialized.size() + 1);
    for (std::size_t i = 0; i < readings_serialized.size(); ++i)
    {
        auto serialized_hex_byte = fmt::format("{:02x}", readings_serialized[i]);
        readings_hex.push_back(serialized_hex_byte[0]);
        readings_hex.push_back(serialized_hex_byte[1]);
    }
    readings_hex.push_back('\0');
    std::printf("%s\n", readings_hex.data());

    readings_ready.emit(std::move(readings_serialized));

    std::printf("%s\n",
        fmt::format("All readings done and sent off to MQTT in {} milliseconds",
            std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now() - start)
                .count())
            .c_str());
}

void ADE9153AReader::timer_callback(void *instance) noexcept
{
    auto self = static_cast<ADE9153AReader *>(instance);
#ifdef __xtensa__
    HEDLEY_DIAGNOSTIC_PUSH
    CM_DIAGNOSTIC_IGNORE_USELESS_CAST

    xTaskNotifyGive(static_cast<TaskHandle_t>(self->read_instant_values_task_));

    HEDLEY_DIAGNOSTIC_POP
#else
    CM_UNUSED(self);
#endif
}

[[noreturn]] void *ADE9153AReader::read_instance_values(void *instance) noexcept
{
    auto self = static_cast<ADE9153AReader *>(instance);

#ifdef __xtensa__
    self->read_instant_values_task_ = xTaskGetCurrentTaskHandle();
    self->read_instant_values_lock_ = xSemaphoreCreateBinary();
    xSemaphoreGive(static_cast<QueueHandle_t>(self->read_instant_values_lock_));
#endif

    for (;;)
    {
#ifdef __xtensa__
        const auto notification_value = ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
#else
        const auto notification_value = 1;
        std::this_thread::sleep_for(std::chrono::microseconds{ 244 });
#endif
        if (notification_value == 1)
        {
            if (xSemaphoreTake(static_cast<QueueHandle_t>(self->read_instant_values_lock_), 1))
            {
                self->ade9153a_.ReadInstantRegs(&self->instant_registers_);

                const auto current_sample = self->instant_registers_.IVal;
                const auto voltage_sample = self->instant_registers_.VVal;

                if (const auto push_success = self->current_samples_.push_one(&current_sample);
                    !push_success)
                {
                    CM_LOG_ERROR("Failed to push current data to sample buffer");
                    continue;
                }

                // Make sure to keep this last
                if (const auto push_success = self->voltage_samples_.push_one(&voltage_sample);
                    !push_success)
                {
                    CM_LOG_ERROR("Failed to push voltage data to sample buffer");
                    continue;
                }

                xSemaphoreGive(static_cast<QueueHandle_t>(self->read_instant_values_lock_));
            }
            else
            {
                CM_LOG_WARN("Window to read missed, could not acquire semaphore");
            }
        }
    }
}

#include <array>

#include "common.hh"
#include "logger.hh"
#include "mqtt_client.hh"

#ifdef __xtensa__

#include <esp_tls.h>

void MQTTClient::connect() noexcept
{
    auto mqtt_config = esp_mqtt_client_config_t{};
    mqtt_config.uri = url_.c_str();
    mqtt_config.username = username_.c_str();
    mqtt_config.password = password_.c_str();
    mqtt_config.port = port_;
    mqtt_config.use_global_ca_store = true;

    client_handle_ = esp_mqtt_client_init(&mqtt_config);
    esp_mqtt_client_register_event(client_handle_,
        static_cast<esp_mqtt_event_id_t>(ESP_EVENT_ANY_ID),
        &MQTTClient::event_handler,
        this);
    esp_mqtt_client_start(client_handle_);
}

void MQTTClient::disconnect() noexcept
{
    esp_mqtt_client_disconnect(client_handle_);
    esp_mqtt_client_destroy(client_handle_);
}

void MQTTClient::subscribe(std::string_view topic) noexcept
{
    esp_mqtt_client_subscribe(client_handle_, topic.data(), 0);
}

MQTTClient::MessageID MQTTClient::publish_message(std::string_view topic,
    const std::uint8_t *message,
    std::size_t length) noexcept
{
    return esp_mqtt_client_publish(client_handle_,
        topic.data(),
        reinterpret_cast<const char *>(message),
        static_cast<int>(length),
        0,
        0);
}

MQTTClient::MessageID MQTTClient::enqueue_message(std::string_view topic,
    const std::uint8_t *message,
    std::size_t length) noexcept
{
    return esp_mqtt_client_enqueue(client_handle_,
        topic.data(),
        reinterpret_cast<const char *>(message),
        static_cast<int>(length),
        0,
        0,
        true);
}

void MQTTClient::event_handler(void *instance,
    esp_event_base_t base,
    std::int32_t event_id,
    void *event_data)
{
    CM_UNUSED(base);

    CM_LOG_INFO_F("MQTT event: {}", event_id);

    auto *self = static_cast<MQTTClient *>(instance);

    esp_mqtt_event_handle_t event = static_cast<esp_mqtt_event_handle_t>(event_data);
    switch (static_cast<esp_mqtt_event_id_t>(event_id))
    {
    case MQTT_EVENT_CONNECTED:
        self->is_connected_ = true;
        self->connected.emit();
        break;
    case MQTT_EVENT_DISCONNECTED:
        self->is_connected_ = false;
        self->disconnected.emit();
        break;
    case MQTT_EVENT_SUBSCRIBED: {
        self->subscribed.emit(event->msg_id);

        break;
    }
    case MQTT_EVENT_UNSUBSCRIBED: {
        self->unsubscribed.emit(event->msg_id);

        break;
    }
    case MQTT_EVENT_PUBLISHED:
        self->published.emit(event->msg_id);
        break;
    case MQTT_EVENT_DATA: {
        const auto event_topic_length = static_cast<std::size_t>(event->topic_len);
        self->received.emit(std::string_view{ event->topic, event_topic_length },
            gsl::span{ reinterpret_cast<std::uint8_t *>(event->data),
                static_cast<std::size_t>(event->data_len) });

        break;
    }
    case MQTT_EVENT_ERROR: {
        static_assert(static_cast<int>(Error::BadProtocol) == MQTT_CONNECTION_REFUSE_PROTOCOL);
        static_assert(static_cast<int>(Error::IDRejected) == MQTT_CONNECTION_REFUSE_ID_REJECTED);
        static_assert(static_cast<int>(Error::ServerUnavailable) ==
                      MQTT_CONNECTION_REFUSE_SERVER_UNAVAILABLE);
        static_assert(static_cast<int>(Error::BadUsername) == MQTT_CONNECTION_REFUSE_BAD_USERNAME);
        static_assert(
            static_cast<int>(Error::NotAuthorized) == MQTT_CONNECTION_REFUSE_NOT_AUTHORIZED);

        if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT)
        {
            self->error.emit(Error::TCPTransportError);
        }
        else if (event->error_handle->error_type == MQTT_ERROR_TYPE_CONNECTION_REFUSED)
        {
            self->error.emit(static_cast<Error>(event->error_handle->connect_return_code));
        }
        else
        {
            self->error.emit(Error::Unknown);
        }

        break;
    }
    default:
        break;
    }
}

#else

void MQTTClient::connect() noexcept
{
    CM_UNUSED(client_handle_);
}

void MQTTClient::disconnect() noexcept
{}

void MQTTClient::subscribe(std::string_view topic) noexcept
{
    CM_UNUSED(topic);
}

MQTTClient::MessageID MQTTClient::publish_message(std::string_view,
    const std::uint8_t *,
    std::size_t) noexcept
{
    return {};
}

MQTTClient::MessageID MQTTClient::enqueue_message(std::string_view,
    const std::uint8_t *,
    std::size_t) noexcept
{
    return {};
}

void MQTTClient::event_handler(void *, esp_event_base_t, std::int32_t, void *)
{}

#endif

void MQTTClient::set_broker_url(std::string_view url, std::uint32_t port) noexcept
{
    url_.clear();
    url_.append(url);
    port_ = port;
}

void MQTTClient::set_username(std::string_view username) noexcept
{
    username_.clear();
    username_.append(username);
}

void MQTTClient::set_password(std::string_view password) noexcept
{
    password_.clear();
    password_.append(password);
}

#ifdef __xtensa__

#include "ade9153a_reader.hh"
#include "common.hh"
#include "logger.hh"
#include "mqtt_client.hh"
#include "resources.hh"
#include "wifi.hh"

#include <string_view>

#include <esp_event.h>
#include <esp_task_wdt.h>
#include <esp_tls.h>
#include <nvs_flash.h>

namespace
{
    constexpr auto PAYLOAD_TARGET_TOPIC =
        std::string_view{ "v1/cEf6JwGsTRrWsrEY/devices/DEADBEEF87981672/0/binary" };

    constexpr auto DEFAULT_INTERVAL_MS = std::chrono::milliseconds{ 60 * 1000 }.count();

    constexpr auto BURST_MODE_TOPIC =
        std::string_view{ "v1/cEf6JwGsTRrWsrEY/devices/EADBEEF87981672/0/config" };
    constexpr auto BURST_MODE_INTERVAL_MS = std::chrono::milliseconds{ 5 * 1000 }.count();
    constexpr auto BURST_MODE_MAX_ACTIVE_TIME_MS = std::chrono::milliseconds{ 20 * 1000 }.count();

    inline void initialize_nvs()
    {
        auto ret = nvs_flash_init();
        if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
        {
            ESP_ERROR_CHECK(nvs_flash_erase());
            ret = nvs_flash_init();
        }
        ESP_ERROR_CHECK(ret);
    }

    inline void initialize_tcp_ip_stack()
    {
        ESP_ERROR_CHECK(esp_netif_init());
    }

    inline void create_main_event_loop()
    {
        ESP_ERROR_CHECK(esp_event_loop_create_default());
    }

    inline void initialize_wifi()
    {
        esp_netif_create_default_wifi_sta();
        const wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
        ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    }

} // namespace

extern "C" void app_main()
{
    initialize_nvs();

    esp_err_t esp_ret = ESP_FAIL;
    esp_ret = esp_tls_set_global_ca_store(CERT_ISRG_X1_START,
        static_cast<std::uint32_t>(CERT_ISRG_X1_END - CERT_ISRG_X1_START));
    if (esp_ret != ESP_OK)
    {
        CM_LOG_ERROR_F("Error in setting the global ca store: [{:x}] ({}),could not complete the "
                       "https_request using global_ca_store",
            esp_ret,
            esp_err_to_name(esp_ret));
        vTaskDelay(5 * 1000 / portTICK_PERIOD_MS);
        std::exit(EXIT_FAILURE);
    }

    initialize_tcp_ip_stack();
    create_main_event_loop();
    initialize_wifi();

    pm_signal::init_queued_signal_handler();

    static auto wifi = WiFi{};
    static auto mqtt_client = MQTTClient{};

    static auto burst_mode_active{ false };
    static auto burst_mode_active_since = std::chrono::system_clock::now();
    static auto last_sent = std::chrono::system_clock::now() - std::chrono::hours{ 1 };

    auto afe_reader = new ADE9153AReader{};
    afe_reader->readings_ready.connect(
        [](std::vector<std::uint8_t> &&readings_serialized) noexcept {
            if (mqtt_client.is_connected())
            {
                const auto time_since_last_sent_ms =
                    std::chrono::duration_cast<std::chrono::milliseconds>(
                        std::chrono::system_clock::now() - last_sent)
                        .count();

                if ((burst_mode_active && time_since_last_sent_ms >= BURST_MODE_INTERVAL_MS) ||
                    (time_since_last_sent_ms >= DEFAULT_INTERVAL_MS))
                {
                    readings_serialized.insert(readings_serialized.cbegin(), 0);
                    readings_serialized.insert(readings_serialized.cbegin(), 0);
                    mqtt_client.publish_message(PAYLOAD_TARGET_TOPIC,
                        readings_serialized.data(),
                        readings_serialized.size());

                    last_sent = std::chrono::system_clock::now();

                    const auto time_since_burst_mode_active =
                        std::chrono::duration_cast<std::chrono::milliseconds>(
                            std::chrono::system_clock::now() - burst_mode_active_since)
                            .count();
                    if (time_since_burst_mode_active > BURST_MODE_MAX_ACTIVE_TIME_MS)
                    {
                        burst_mode_active = false;
                    }
                }
            }
            else
            {
                CM_LOG_WARN("No MQTT connection available. Cannot send readings.");
            }
        },
        pm_signal::ConnectionType::Queued);

    wifi.connected.connect([](std::uint32_t ip_address) {
        CM_LOG_INFO_F("Connected to access point with SSID: \"{}\"", WIFI_SSID);

        const auto esp_ip_addr = esp_ip4_addr_t{ ip_address };
        CM_LOG_INFO_F("Ip address: {}.{}.{}.{}", IP2STR(&esp_ip_addr));

        CM_LOG_INFO("Connecting to MQTT broker...");
        mqtt_client.set_broker_url(MQTT_BROKER_URL, MQTT_TLS_PORT);
        mqtt_client.set_username(MQTT_USERNAME);
        mqtt_client.set_password(MQTT_PASSWORD);
        mqtt_client.connect();
    });

    mqtt_client.connected.connect(
        [](ADE9153AReader *afe) {
            CM_LOG_INFO_F("MQTT: Connected to {}...", MQTT_BROKER_URL);
            afe->start_reading();
            mqtt_client.subscribe(BURST_MODE_TOPIC);
        },
        *afe_reader);

    mqtt_client.subscribed.connect([](MQTTClient::MessageID) noexcept {
        CM_LOG_INFO("MQTT subscribed to topic");
    });

    mqtt_client.received.connect([](std::string_view topic, auto data) {
        CM_UNUSED(data);
        if (topic == BURST_MODE_TOPIC)
        {
            CM_LOG_INFO_F("Toggling burst mode {}...", (burst_mode_active ? "OFF" : "ON"));
            burst_mode_active_since = std::chrono::system_clock::now();
            burst_mode_active = !burst_mode_active;
        }
    });

    wifi.connection_failed.connect([]() {
        CM_LOG_ERROR_F("Failed to connect to Access Point \"{}\"", WIFI_SSID);
        vTaskDelay((5 * 1000) / portTICK_PERIOD_MS);
        std::exit(EXIT_FAILURE);
    });

    wifi.disconnected.connect([]() {
        CM_LOG_INFO("WiFi disconnected. Trying to reconnect...");
        mqtt_client.disconnect();
        wifi.connect();
    });

    CM_LOG_INFO("Connecting to WiFi access point...");
    wifi.set_ssid(WIFI_SSID);
    wifi.set_password(WIFI_PASSWORD);
    wifi.set_authentication(WiFi::AuthenticationMode::WPA2_PSK);
    wifi.connect();

    for (;;)
    {
        vTaskDelay(portMAX_DELAY);
    }
}

#else

#include <blockingconcurrentqueue.h>

#include <functional>
#include <memory>

static auto main_queue_instance = moodycamel::BlockingConcurrentQueue<std::function<void()> *>{};

void main_queue_enqueue(std::function<void()> *callback)
{
    main_queue_instance.enqueue(callback);
}

int main()
{
    for (;;)
    {
        std::function<void()> *callback = nullptr;
        main_queue_instance.wait_dequeue(callback);
        {
            std::unique_ptr<std::function<void()>> ptr{ callback };
            (*callback)();
        }
    }
}
#endif /* __xtensa__ */

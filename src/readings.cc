#include "readings.hh"

std::vector<std::uint8_t> Readings::to_binary()
{
    std::vector<std::uint8_t> result;
    result.reserve(213);

    rms_voltage_.serialize_to(result);
    rms_current_.serialize_to(result);
    total_active_power_.serialize_to(result);
    apparent_power_.serialize_to(result);
    fundamental_reactive_power_.serialize_to(result);
    power_factor_.serialize_to(result);
    total_active_energy_.serialize_to(result);
    apparent_energy_.serialize_to(result);
    fundamental_reactive_energy_.serialize_to(result);
    frequency_.serialize_to(result);
    phi_.serialize_to(result);
    voltage_thd_.serialize_to(result);
    current_thd_.serialize_to(result);

    for (auto v : voltage_hd_)
    {
        v.serialize_to(result);
    }

    for (auto v : current_hd_)
    {
        v.serialize_to(result);
    }

    return result;
}

Readings Readings::from_binary(const std::uint8_t *payload) noexcept
{
    auto result = Readings{};

    result.rms_voltage_.deserialize_from(payload);
    result.rms_current_.deserialize_from(payload);
    result.total_active_power_.deserialize_from(payload);
    result.apparent_power_.deserialize_from(payload);
    result.fundamental_reactive_power_.deserialize_from(payload);
    result.power_factor_.deserialize_from(payload);
    result.total_active_energy_.deserialize_from(payload);
    result.apparent_energy_.deserialize_from(payload);
    result.fundamental_reactive_energy_.deserialize_from(payload);
    result.frequency_.deserialize_from(payload);
    result.phi_.deserialize_from(payload);
    result.voltage_thd_.deserialize_from(payload);
    result.current_thd_.deserialize_from(payload);

    for (auto &v : result.voltage_hd_)
    {
        v.deserialize_from(payload);
    }

    for (auto &v : result.current_hd_)
    {
        v.deserialize_from(payload);
    }

    return result;
}

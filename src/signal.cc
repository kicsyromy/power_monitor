#include "signal.hh"

#ifdef __xtensa__

#include <FreeRTOS.h>
#include <esp_event.h>
#include <freertos/event_groups.h>

esp_event_base_t SIGNAL_EVENT_BASE{};

namespace
{
    void queued_signal_handler(void *, esp_event_base_t, std::int32_t, void *event_data) noexcept
    {
        auto cb = *static_cast<std::function<void()> **>(event_data);

        CM_LOG_DEBUG_F("Sending signal via callback {}...", static_cast<void *>(cb));
        (*cb)();

        delete cb;
    }
} // namespace
#else
void main_queue_enqueue(std::function<void()> *callback);
#endif

void pm_signal::init_queued_signal_handler() noexcept
{
#ifdef __xtensa__
    SIGNAL_EVENT_BASE = "SIGNAL_EVENT_BASE";

    ESP_ERROR_CHECK(esp_event_handler_instance_register(SIGNAL_EVENT_BASE,
        0,
        &queued_signal_handler,
        nullptr,
        nullptr));
#else
#endif
}

void pm_signal::internal::post_signal_event(std::function<void()> *callback) noexcept
{
#ifdef __xtensa__
    CM_LOG_DEBUG_F("Posting message with callback {}...", static_cast<void *>(callback));
    const auto result = esp_event_post(SIGNAL_EVENT_BASE, 0, &callback, sizeof(callback), 1);
    if (result != ESP_OK)
    {
        CM_LOG_ERROR_F("Failed to emit queued signal; ESP32 error: {}", esp_err_to_name(result));
        delete callback;
    }
#else
    main_queue_enqueue(callback);
#endif
}

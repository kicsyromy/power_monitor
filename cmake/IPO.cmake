option(CM_ENABLE_IPO "Enable Iterprocedural Optimization, aka Link Time Optimization (LTO)" ON)

if (CM_ENABLE_IPO)
    include(CheckIPOSupported)
    check_ipo_supported(RESULT CM_IPO_SUPPORTED OUTPUT output)
endif ()

function(enable_ipo project_name)
    if (CM_ENABLE_IPO)
        if (CM_IPO_SUPPORTED)
            message(STATUS "[CurrentMonitor] Enabling IPO for ${project_name}")
            set_property(
                TARGET ${project_name}
                PROPERTY INTERPROCEDURAL_OPTIMIZATION ON
            )
        else ()
            message(SEND_ERROR "[CurrentMonitor] IPO is not supported: ${output}")
        endif ()
    endif ()
endfunction()

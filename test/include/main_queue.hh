#pragma once

#include <functional>

void main_queue_enqueue(std::function<void()> *callback);
void main_queue_run_once();

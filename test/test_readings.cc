#include <catch2/catch.hpp>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wkeyword-macro"
#endif
#define private public
#include "readings.cc"
#ifdef __clang__
#pragma clang diagnostic pop
#endif

#include "common.hh"

#include <fmt/format.h>

namespace
{
    constexpr double EPSILON{ 4.94065645841247E-324 };
}

TEST_CASE("Readings::set_rms_voltage", "[readings]")
{
    using UnderlyingType = decltype(Readings::rms_voltage_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ 213230.032F };
    constexpr auto EXPECTED{ static_cast<UnderlyingType>(ORIGINAL_VALUE) };

    Readings readings{};
    readings.set_rms_voltage(ORIGINAL_VALUE);
    REQUIRE(readings.rms_voltage_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_rms_current", "[readings]")
{
    using UnderlyingType = decltype(Readings::rms_current_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ 123360.032F };
    constexpr auto EXPECTED{ static_cast<UnderlyingType>(ORIGINAL_VALUE) };

    Readings readings{};
    readings.set_rms_current(ORIGINAL_VALUE);
    REQUIRE(readings.rms_current_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_total_active_power", "[readings]")
{
    using UnderlyingType = decltype(Readings::total_active_power_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ static_cast<float>(std::numeric_limits<UnderlyingType>::max()) +
                                   0.036F };
    constexpr auto EXPECTED{ std::numeric_limits<UnderlyingType>::max() };

    Readings readings{};
    readings.set_total_active_power(ORIGINAL_VALUE);
    REQUIRE(readings.total_active_power_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_apparent_power", "[readings]")
{
    using UnderlyingType = decltype(Readings::apparent_power_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ 4001332.036F };
    constexpr auto EXPECTED = UnderlyingType{ 4001332 };

    Readings readings{};
    readings.set_apparent_power(ORIGINAL_VALUE);
    REQUIRE(readings.apparent_power_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_fundamental_reactive_power", "[readings]")
{
    using UnderlyingType = decltype(Readings::fundamental_reactive_power_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ static_cast<float>(std::numeric_limits<UnderlyingType>::min()) -
                                   0.036F };
    constexpr auto EXPECTED{ std::numeric_limits<UnderlyingType>::min() };

    Readings readings{};
    readings.set_fundamental_reactive_power(ORIGINAL_VALUE);
    REQUIRE(readings.fundamental_reactive_power_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_power_factor", "[readings]")
{
    using UnderlyingType = decltype(Readings::power_factor_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ -0.89F };
    constexpr auto EXPECTED = UnderlyingType{ -89 };

    Readings readings{};
    readings.set_power_factor(ORIGINAL_VALUE);
    REQUIRE(readings.power_factor_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_total_active_energy", "[readings]")
{
    using UnderlyingType = decltype(Readings::total_active_energy_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ 173709557760.036F };
    constexpr auto EXPECTED = UnderlyingType{ 173709557760 };

    Readings readings{};
    readings.set_total_active_energy(ORIGINAL_VALUE);
    REQUIRE(readings.total_active_energy_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_apparent_energy", "[readings]")
{
    using UnderlyingType = decltype(Readings::apparent_energy_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ 80996.036F };
    constexpr auto EXPECTED = UnderlyingType{ 80996 };

    Readings readings{};
    readings.set_apparent_energy(ORIGINAL_VALUE);
    REQUIRE(readings.apparent_energy_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_fundamental_reactive_energy", "[readings]")
{
    using UnderlyingType = decltype(Readings::fundamental_reactive_energy_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ 58443.036F };
    constexpr auto EXPECTED = UnderlyingType{ 58443 };

    Readings readings{};
    readings.set_fundamental_reactive_energy(ORIGINAL_VALUE);
    REQUIRE(readings.fundamental_reactive_energy_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_frequency", "[readings]")
{
    using UnderlyingType = decltype(Readings::frequency_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ 49.999992F };
    constexpr auto EXPECTED = UnderlyingType{ 49999 };

    Readings readings{};
    readings.set_frequency(ORIGINAL_VALUE);
    REQUIRE(readings.frequency_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_phi", "[readings]")
{
    using UnderlyingType = decltype(Readings::phi_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ 0.66F };
    constexpr auto EXPECTED = UnderlyingType{ 66 };

    Readings readings{};
    readings.set_phi(ORIGINAL_VALUE);
    REQUIRE(readings.phi_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_voltage_thd", "[readings]")
{
    using UnderlyingType = decltype(Readings::voltage_thd_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ 12.34F };
    constexpr auto EXPECTED = UnderlyingType{ 1234 };

    Readings readings{};
    readings.set_voltage_thd(ORIGINAL_VALUE);
    REQUIRE(readings.voltage_thd_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_current_thd", "[readings]")
{
    using UnderlyingType = decltype(Readings::current_thd_)::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE{ 12.34F };
    constexpr auto EXPECTED = UnderlyingType{ 1234 };

    Readings readings{};
    readings.set_current_thd(ORIGINAL_VALUE);
    REQUIRE(readings.current_thd_.base_value() == EXPECTED);
}

TEST_CASE("Readings::set_voltage_hd", "[readings]")
{
    using UnderlyingType =
        std::remove_pointer_t<decltype(Readings::voltage_hd_.data())>::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE = std::array{ 12.34F, .0F };
    constexpr auto EXPECTED = UnderlyingType{ 10000 };

    Readings readings{};
    const auto values =
        std::vector<std::array<float, 2>>(51 * readings.voltage_hd_.size(), ORIGINAL_VALUE);
    readings.set_voltage_hd(reinterpret_cast<Readings::Complex const *>(values.data()),
        values.size());
    for (auto v : readings.voltage_hd_)
    {
        REQUIRE(v.base_value() == EXPECTED);
    }
    REQUIRE(static_cast<double>(std::abs(readings.voltage_thd_.base_value() - 177.F)) < EPSILON);
}

TEST_CASE("Readings::set_current_hd", "[readings]")
{
    using UnderlyingType =
        std::remove_pointer_t<decltype(Readings::current_hd_.data())>::Type::UnderlyingType;

    constexpr auto ORIGINAL_VALUE = std::array{ 12.34F, .0F };
    constexpr auto EXPECTED = UnderlyingType{ 10000 };

    Readings readings{};
    const auto values =
        std::vector<std::array<float, 2>>(51 * readings.current_hd_.size(), ORIGINAL_VALUE);
    readings.set_current_hd(reinterpret_cast<Readings::Complex const *>(values.data()),
        values.size());
    for (auto v : readings.current_hd_)
    {
        REQUIRE(v.base_value() == EXPECTED);
    }
    REQUIRE(static_cast<double>(std::abs(readings.current_thd_.base_value() - 177.F)) < EPSILON);
}

TEST_CASE("Readings::to_binary", "[readings]")
{
    Readings readings{};

    {
        using UnderlyingType = decltype(Readings::rms_voltage_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{ 213230.032F };
        constexpr auto EXPECTED{ static_cast<UnderlyingType>(ORIGINAL_VALUE) };

        readings.set_rms_voltage(ORIGINAL_VALUE);
        REQUIRE(readings.rms_voltage_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType = decltype(Readings::rms_current_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{ 123360.032F };
        constexpr auto EXPECTED{ static_cast<UnderlyingType>(ORIGINAL_VALUE) };

        readings.set_rms_current(ORIGINAL_VALUE);
        REQUIRE(readings.rms_current_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType = decltype(Readings::total_active_power_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{
            static_cast<float>(std::numeric_limits<UnderlyingType>::max()) + 0.036F
        };
        constexpr auto EXPECTED{ std::numeric_limits<UnderlyingType>::max() };

        readings.set_total_active_power(ORIGINAL_VALUE);
        REQUIRE(readings.total_active_power_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType = decltype(Readings::apparent_power_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{ 4001332.036F };
        constexpr auto EXPECTED = UnderlyingType{ 4001332 };

        readings.set_apparent_power(ORIGINAL_VALUE);
        REQUIRE(readings.apparent_power_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType =
            decltype(Readings::fundamental_reactive_power_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{
            static_cast<float>(std::numeric_limits<UnderlyingType>::min()) - 0.036F
        };
        constexpr auto EXPECTED{ std::numeric_limits<UnderlyingType>::min() };

        readings.set_fundamental_reactive_power(ORIGINAL_VALUE);
        REQUIRE(readings.fundamental_reactive_power_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType = decltype(Readings::power_factor_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{ -0.89F };
        constexpr auto EXPECTED = UnderlyingType{ -89 };

        readings.set_power_factor(ORIGINAL_VALUE);
        REQUIRE(readings.power_factor_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType = decltype(Readings::total_active_energy_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{ 173709557760.036F };
        constexpr auto EXPECTED = UnderlyingType{ 173709557760 };

        readings.set_total_active_energy(ORIGINAL_VALUE);
        REQUIRE(readings.total_active_energy_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType = decltype(Readings::apparent_energy_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{ 80996.036F };
        constexpr auto EXPECTED = UnderlyingType{ 80996 };

        readings.set_apparent_energy(ORIGINAL_VALUE);
        REQUIRE(readings.apparent_energy_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType =
            decltype(Readings::fundamental_reactive_energy_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{ 58443.036F };
        constexpr auto EXPECTED = UnderlyingType{ 58443 };

        readings.set_fundamental_reactive_energy(ORIGINAL_VALUE);
        REQUIRE(readings.fundamental_reactive_energy_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType = decltype(Readings::frequency_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{ 50.41F };
        constexpr auto EXPECTED = UnderlyingType{ 50410 };

        readings.set_frequency(ORIGINAL_VALUE);
        REQUIRE(readings.frequency_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType = decltype(Readings::phi_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{ 0.66F };
        constexpr auto EXPECTED = UnderlyingType{ 66 };

        readings.set_phi(ORIGINAL_VALUE);
        REQUIRE(readings.phi_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType =
            std::remove_pointer_t<decltype(Readings::voltage_hd_.data())>::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE = std::array{ 12.34F, .0F };
        constexpr auto EXPECTED = UnderlyingType{ 10000 };

        const auto values =
            std::vector<std::array<float, 2>>(51 * readings.voltage_hd_.size(), ORIGINAL_VALUE);
        readings.set_voltage_hd(reinterpret_cast<Readings::Complex const *>(values.data()),
            values.size());
        for (auto &v : readings.voltage_hd_)
        {
            REQUIRE(v.base_value() == EXPECTED);
            set_value(v, 12.34F);
        }
        REQUIRE(
            static_cast<double>(std::abs(readings.voltage_thd_.base_value() - 177.F)) < EPSILON);
    }
    {
        using UnderlyingType =
            std::remove_pointer_t<decltype(Readings::current_hd_.data())>::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE = std::array{ 12.34F, .0F };
        constexpr auto EXPECTED = UnderlyingType{ 10000 };

        const auto values =
            std::vector<std::array<float, 2>>(51 * readings.current_hd_.size(), ORIGINAL_VALUE);
        readings.set_current_hd(reinterpret_cast<Readings::Complex const *>(values.data()),
            values.size());
        for (auto &v : readings.current_hd_)
        {
            REQUIRE(v.base_value() == EXPECTED);
            set_value(v, 12.34F);
        }
        REQUIRE(
            static_cast<double>(std::abs(readings.current_thd_.base_value() - 177.F)) < EPSILON);

        readings.current_hd_[39].value = 6666;
    }
    {
        using UnderlyingType = decltype(Readings::voltage_thd_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{ 12.34F };
        constexpr auto EXPECTED = UnderlyingType{ 1234 };

        readings.set_voltage_thd(ORIGINAL_VALUE);
        REQUIRE(readings.voltage_thd_.base_value() == EXPECTED);
    }
    {
        using UnderlyingType = decltype(Readings::current_thd_)::Type::UnderlyingType;

        constexpr auto ORIGINAL_VALUE{ 12.34F };
        constexpr auto EXPECTED = UnderlyingType{ 1234 };

        readings.set_current_thd(ORIGINAL_VALUE);
        REQUIRE(readings.current_thd_.base_value() == EXPECTED);
    }

    constexpr auto EXPECTED_BINARY_HEX_STRING =
        "000340ee0001e1e07fffffff003d0e3480000000a70000002871e580000000000000013c64000000000000"
        "e44bc4ea004204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204"
        "d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d2"
        "04d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204"
        "d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d204d21a0a";

    const auto to_byte_array = [](std::string_view byte_array) -> std::vector<std::uint8_t> {
        if (byte_array.size() % 2 != 0)
        {
            return {};
        }

        auto result = std::vector<std::uint8_t>(byte_array.size() / 2);
        for (std::size_t index = 0; index < result.size(); ++index)
        {
            const auto byte_value = std::string{ byte_array.substr(index * 2, 2) };
            result[index] =
                static_cast<std::uint8_t>(std::strtoul(byte_value.c_str(), nullptr, 16));
        }

        return result;
    };

    auto readings_serialized_hex_string = std::string{};

    const auto readings_serialized = readings.to_binary();
    REQUIRE(strlen(EXPECTED_BINARY_HEX_STRING) / 2 == readings_serialized.size());

    for (std::size_t i = 0; i < readings_serialized.size(); ++i)
    {
        auto expected_hex_byte = std::array<char, 2>{};
        expected_hex_byte[0] = EXPECTED_BINARY_HEX_STRING[i * 2];
        expected_hex_byte[1] = EXPECTED_BINARY_HEX_STRING[i * 2 + 1];

        auto serialized_hex_byte = fmt::format("{:02x}", readings_serialized[i]);
        if (strncmp(serialized_hex_byte.c_str(), expected_hex_byte.data(), 2) != 0)
        {
            std::printf("%s != %.*s\n", serialized_hex_byte.c_str(), 2, expected_hex_byte.data());

            auto lhs = Readings::from_binary(readings_serialized.data());
            CM_UNUSED(lhs);
            auto rhs = Readings::from_binary(to_byte_array(EXPECTED_BINARY_HEX_STRING).data());
            CM_UNUSED(rhs);

            REQUIRE(false);
        }

        readings_serialized_hex_string += serialized_hex_byte;
    }

    REQUIRE(readings_serialized_hex_string == EXPECTED_BINARY_HEX_STRING);
}

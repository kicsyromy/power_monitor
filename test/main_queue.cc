#include "main_queue.hh"

#include <blockingconcurrentqueue.h>

#include <memory>

static auto main_queue_instance = moodycamel::BlockingConcurrentQueue<std::function<void()> *>{};

void main_queue_enqueue(std::function<void()> *callback)
{
    main_queue_instance.enqueue(callback);
}

void main_queue_run_once()
{
    std::function<void()> *callback = nullptr;
    main_queue_instance.wait_dequeue(callback);
    {
        std::unique_ptr<std::function<void()>> ptr{ callback };
        (*callback)();
    }
}

#include <catch2/catch.hpp>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wkeyword-macro"
#endif
#define private public
#include "sample_buffer.hh"
#ifdef __clang__
#pragma clang diagnostic pop
#endif

namespace
{
    constexpr auto EPSILON{ 4.94065645841247E-45f };
}

TEST_CASE("SampleBuffer<T, std::size_t>::push_one", "[samplebuffer]")
{
    auto buffer = SampleBuffer<float, 2>{};

    bool buffer_swapped = false;
    buffer.buffer_full.connect(
        [](bool *bs) {
            *bs = true;
        },
        buffer_swapped);

    const auto value = 3.5F;
    auto success = buffer.push_one(&value);

    REQUIRE(success);
    REQUIRE(buffer_swapped == false);
    REQUIRE(std::abs(buffer.active_buffer()[0] - 3.5F) < EPSILON);
}

TEST_CASE("SampleBuffer<T, std::size_t>::push_many", "[samplebuffer]")
{
    auto buffer = SampleBuffer<float, 2>{};

    bool buffer_swapped = false;
    buffer.buffer_full.connect(
        [](bool *bs) {
            *bs = true;
        },
        buffer_swapped);

    const float value[2]{ 3.5F, 2.4F };
    auto success = buffer.push_many(value, 2);

    REQUIRE(success);
    REQUIRE(buffer_swapped == true);
    REQUIRE(std::abs(buffer.inactive_buffer()[0] - 3.5F) < EPSILON);
    REQUIRE(std::abs(buffer.inactive_buffer()[1] - 2.4F) < EPSILON);
}

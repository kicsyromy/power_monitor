#pragma once

#include <cstdint>

enum PinState
{
    HIGH = 1,
    LOW = 0,
};

enum PinMode
{
    INPUT = 0x00000001,
    OUTPUT = 0x00000002,
};

void digitalWrite(std::uint8_t pin, PinState value) noexcept;
void pinMode(std::uint8_t pin, PinMode value) noexcept;
void delay(std::uint32_t milliseconds) noexcept;

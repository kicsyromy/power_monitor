#ifndef __xtensa__

#include "spi.hh"

#include <cstring>

ArduinoSPI SPI{};

void ArduinoSPI::begin() noexcept
{}

void ArduinoSPI::end() noexcept
{}

void ArduinoSPI::beginTransaction([[maybe_unused]] Settings settings) noexcept
{}

void ArduinoSPI::endTransaction() noexcept
{}

std::uint16_t ArduinoSPI::transfer16([[maybe_unused]] std::uint16_t data) noexcept
{
    return 0;
}

void ArduinoSPI::serialize_16_msb(std::uint16_t data, std::uint8_t *buffer) noexcept
{
    buffer[0] = static_cast<std::uint8_t>((data >> 8) & 0xff);
    buffer[1] = static_cast<std::uint8_t>(data & 0xff);
}

void ArduinoSPI::serialize_16_lsb(std::uint16_t data, std::uint8_t *buffer) noexcept
{
    buffer[0] = static_cast<std::uint8_t>(data & 0xff);
    buffer[1] = static_cast<std::uint8_t>((data >> 8) & 0xff);
}

std::uint16_t ArduinoSPI::deserialize_16_msb(std::uint8_t *buffer) noexcept
{
    auto result = std::uint16_t{};
    result = std::uint16_t(buffer[0] << 8);
    result = std::uint16_t(result | buffer[1]);

    return result;
}

std::uint16_t ArduinoSPI::deserialize_16_lsb(std::uint8_t *buffer) noexcept
{
    auto result = std::uint16_t{};
    result = std::uint16_t(buffer[1] << 8);
    result = std::uint16_t(result | buffer[0]);

    return result;
}

#endif

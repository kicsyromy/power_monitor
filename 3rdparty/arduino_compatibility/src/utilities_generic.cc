#ifndef __xtensa__

#include "utilities.hh"

#include <chrono>
#include <thread>

void digitalWrite(std::uint8_t pin, PinState value) noexcept
{}

void pinMode(std::uint8_t pin, PinMode mode) noexcept
{}

void delay(std::uint32_t milliseconds) noexcept
{
    std::this_thread::sleep_for(std::chrono::milliseconds{ milliseconds });
}

#endif
